window.addEventListener('load', function(){
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');
    canvas.width = 1024;
    canvas.height = 576;
    let obstacles = [];
    let birds = [];
    let bottles = [];
    let boards = [];
    let score = 0;
    let bottlesCaught = 0;
    let level = 1;
    let speedUpgrade = 0;
    let gameStart = false;
    let gameOver = false;
    let music = new Audio("ressources/sound/bgmusic.ogg");
    let rockSnd = new Audio("ressources/sound/rock.ogg");
    let bottleSnd = new Audio("ressources/sound/bottle_sound.mp3");
    let whistle = new Audio("ressources/sound/whistle.ogg");

    /* ---- CLASSES & GAME OBJECT RELATED FUNCTIONS ---- */
    class InputHandler {
        constructor(){
            this.keys = [];
            window.addEventListener('keydown', e => {
                if (    e.key === 'ArrowUp'
                        && this.keys.indexOf(e.key) === -1){
                    this.keys.push(e.key);
                }
            });
            window.addEventListener('keyup', e => {
                if (    e.key === 'ArrowUp'){
                    this.keys.splice(this.keys.indexOf(e.key), 1);
                }
            });
        }
    }
    class Badboy {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 200;
            this.height = 200;
            this.x = 10;
            this.y = this.gameHeight - this.height;
            this.image = document.getElementById('badboy');
            this.frameX = 0;
            this.maxFrame = 7;
            this.fps = 20;
            this.frameTimer = 0;
            this.frameInterval = 1000/this.fps;
            this.frameY = 0;
            this.speed = 0;
            this.vy = 0;
            this.weight = 1;
        }
        draw(context){
            //context.fillRect(this.x, this.y, this.width, this.height);
            //context.strokeStyle = 'white';
            context.beginPath();
            context.arc(this.x + this.width/2, this.y + this.height/2.5 + 30, this.width/3, 0, Math.PI *2);
            //context.stroke();
            context.drawImage(this.image, this.frameX * this.width, this.frameY * this.height, this.width, this.height, this.x, this.y, this.width, this.height);
        }
        update(input, deltaTime){
            // sprite animation
            if (this.frameTimer > this.frameInterval){
                if (this.frameX >= this.maxFrame) this.frameX = 0;
                else this.frameX ++;
                this.frameTimer = 0;
            } else {
                this.frameTimer += deltaTime;
            }
        }
    }
    class Coach {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 200;
            this.height = 200;
            this.x = 300;
            this.y = this.gameHeight - this.height;
            this.image = document.getElementById('coach');
            this.frameX = 0;
            this.maxFrame = 7;
            this.fps = 20;
            this.frameTimer = 0;
            this.frameInterval = 1000/this.fps;
            this.frameY = 0;
            this.vy = 0;
            this.weight = 0.8;
        }
        draw(context){
            context.beginPath();
            context.arc(this.x + this.width/2, this.y + this.height/2.5 + 30, this.width/3, 0, Math.PI *2);
            context.drawImage(this.image, this.frameX * this.width, this.frameY * this.height, this.width, this.height, this.x, this.y, this.width, this.height);
        }
        update(input, deltaTime, obstacles, birds, bottles, badboy){
            // collision
            obstacles.forEach(obstacle => {
                const dx = obstacle.x - this.x;
                const dy = obstacle.y - this.y;
                const distance = Math.sqrt(dx * dx + dy * dy);
                if (distance < obstacle.width/2 + this.width/2 && obstacle.touch===100){
                    obstacle.touch=99;
                    rockSnd.play();
                    repeat(backward,30);
                }
            });
            birds.forEach(bird => {
                const dx2 = bird.x - this.x;
                const dy2 = bird.y - this.y;
                const distance1 = Math.sqrt(dx2 * dx2 + dy2 * dy2);
                if (distance1 < bird.width/2 + this.width/2 && bird.touch===100){
                    bird.touch=99;
                    rockSnd.play();
                    repeat(backward,10);
                }
            });
            bottles.forEach(bottle => {
                const dx1 = bottle.x - this.x;
                const dy1 = bottle.y - this.y;
                const distance2 = Math.sqrt(dx1 * dx1 + dy1 * dy1);
                if (distance2 < bottle.width/2 + this.width/2 && this.y == 62.400000000000205){
                    bottle.x = -100;
                    repeat(forward, 10);
                    bottleSnd.play();
                    bottlesCaught++;
                }
            });
            const dx2 = badboy.x - this.x;
            const dy2 = badboy.y - this.y;
            const distance2 = Math.sqrt(dx2 * dx2 + dy2 * dy2);
            if (distance2 < badboy.width/2 - 10 || coach.x < 10){
                gameOver = true;
            }
            // sprite animation
            if (this.frameTimer > this.frameInterval){
                if (this.frameX >= this.maxFrame) this.frameX = 0;
                else this.frameX ++;
                this.frameTimer = 0;
            } else {
                this.frameTimer += deltaTime;
            }
            // controls
            if (input.keys.indexOf('ArrowUp') > -1 && this.onGround()){
                this.vy -= 22;
            }
            // vertical movement
            this.y += this.vy;
            if (!this.onGround()){
                this.vy += this.weight;
            } else {
                this.vy = 0;
            }
            if (this.y > this.gameHeight - this.height){
              this.y = this.gameHeight - this.height;
            }
        }
        onGround(){
            return this.y >= this.gameHeight - this.height;
        }
    }
    class Background {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('background');
            this.width = 3072;
            this.heigth = 576;
            this.speed = 6 + speedUpgrade;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
            context.drawImage(this.image, this.x + this.width - this.speed, this.y, this.width, this.heigth);
        }
        update(){
            this.x -= this.speed;
            if(this.x < 0 - this.width){
                this.x = 0;
            }
        }
    }
    class Background1 {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('background1');
            this.width = 3072;
            this.heigth = 576;
            this.speed = 5.5 + speedUpgrade;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
            context.drawImage(this.image, this.x + this.width - this.speed, this.y, this.width, this.heigth);
        }
        update(){
            this.x -= this.speed;
            if(this.x < 0 - this.width){
                this.x = 0;
            }
        }
    }
    class BackgroundEnd {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('backgroundend');
            this.width = 1024;
            this.heigth = 576;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
        }
    }
    class Stadium {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('stadium');
            this.width = 1024;
            this.heigth = 576;
            this.speed = 6 + speedUpgrade;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
            context.drawImage(this.image, this.x + this.width - this.speed, this.y, this.width, this.heigth);
        }
        update(){
            /*this.x -= this.speed;
            if(this.x < 0 - this.width){
                this.x = 0;
            }*/
        }
    }
    class Obstacle {
        constructor(gameWidth, gameHeight, frameX){
            this.touch=100;
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 100;
            this.height = 84;
            this.image = document.getElementById('obstacle');
            this.x = this.gameWidth;
            this.y = this.gameHeight - this.height - 10;
            this.frameX = frameX;
            this.frameY = 0;
            this.speed = 7 + speedUpgrade;
        }
        draw(context){
            context.beginPath();
            context.arc(this.x + this.width/2, this.y + this.height/2, this.width/2, 0, Math.PI * 2);
            context.drawImage(this.image, this.frameX * this.width, this.frameY * this.height, this.width, this.height, this.x, this.y, this.width, this.height);
        }
        update(){
            this.x -= this.speed;
        }
    }
    class Bird {
        constructor(gameWidth, gameHeight){
            this.touch=100;
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 100;
            this.height = 84;
            this.image = document.getElementById('bird');
            this.x = this.gameWidth;
            this.y = this.gameHeight - this.height - 340;
            this.vy = 1;
            this.weight = 0.8;
            this.speed = 7 + speedUpgrade;
        }
        draw(context){
            context.beginPath();
            context.arc(this.x + this.width/2, this.y + this.height/2, this.width/2, 0, Math.PI * 2);
            context.drawImage(this.image, this.x, this.y, this.width, this.height);
        }
        update(){
            this.x -= this.speed;
            this.y -= this.vy;
        }
    }
    class Bottle {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 100;
            this.height = 112;
            this.image = document.getElementById('bottle');
            this.x = this.gameWidth;
            this.y = this.gameHeight - (this.height * 4.5);
            this.speed = 8 + speedUpgrade;
        }
        draw(context){
            context.beginPath();
            context.arc(this.x + this.width/2, this.y + this.height/2, this.width/2, 0, Math.PI * 2);
            context.drawImage(this.image, this.x, this.y, this.width, this.height);

        }
        update(){
            this.x -= this.speed;
        }
    }
    class Board {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.width = 200;
            this.height = 200;
            this.image = document.getElementById('board');
            this.frameX = level-2;
            this.frameY = 0;
            this.x = this.gameWidth;
            this.y = this.gameHeight - this.height - 40;
            this.speed = 6 + speedUpgrade;
        }
        draw(context){
            context.drawImage(this.image, this.frameX * this.width, this.frameY * this.height, this.width, this.height, this.x, this.y, this.width, this.height);
        }
        update(){
            this.x -= this.speed;
        }
    }
    class Score {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('score');
            this.width = 1024;
            this.heigth = 576;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
        }
    }
    class HowToPlay {
        constructor(gameWidth, gameHeight){
            this.gameWidth = gameWidth;
            this.gameHeight = gameHeight;
            this.x = 0;
            this.y = 0;
            this.image = document.getElementById('howtoplay');
            this.width = 1024;
            this.heigth = 576;
        }
        draw(context){
            context.drawImage(this.image, this.x, this.y, this.width, this.heigth);
        }
    }

    function handleObstacles(deltaTime) {
        if (obstacleTimer > obstacleInterval + randomObstacleInterval){
            obstacles.push(new Obstacle(canvas.width, canvas.height, Math.round(Math.random()*2)));
            randomObstacleInterval = Math.random() * 1500 + 500;
            obstacleTimer = 0;
        } else {
            obstacleTimer += deltaTime;
        }
        obstacles.forEach(obstacle => {
            obstacle.draw(ctx);
            obstacle.update();
        });
    }
    function handleBirds(deltaTime) {
        if (birdTimer > birdInterval + randomBirdInterval){
            obstacles.forEach(obstacle => {
                birds.push(new Bird(canvas.width, canvas.height));
                randomBirdInterval = Math.random() * 3500 + 500;
                birdTimer = 0;
            });
        } else {
            birdTimer += deltaTime;
        }
        birds.forEach(bird => {
            bird.draw(ctx);
            bird.update();
        });
    }
    function handleBottles(deltaTime) {
        if (bottleTimer > bottleInterval + randomBottleInterval){
            bottles.push(new Bottle(canvas.width, canvas.height));
            randomBottleInterval = Math.random() * 2000 + 500;
            bottleTimer = 0;
        } else {
            bottleTimer += deltaTime;
        }
        bottles.forEach(bottle => {
            bottle.draw(ctx);
            bottle.update();
        });
    }
    function handleBoards() {
        if (score == 1000 || score == 2000 || score == 3000 || score == 4000 || score == 4000 || score == 5000 || score == 6000 || score == 7000 || score == 8000 || score == 9000 || score == 10000){
            boards.push(new Board(canvas.width, canvas.height));
        }
        boards.forEach(board => {
            board.draw(ctx);
            board.update();
        });
    }
    function handleSpeed(){
        if (score == 1000 * level && score < 3000){
            speedUpgrade += 1.5;
            level ++;
        }
         if (score == 1000 * level && score < 6000){
            speedUpgrade += 1;
            level ++;
        }
         if (score == 1000 * level && score < 10000){
            speedUpgrade += 0.5;
            level ++;
        }
        if (score == 1000 * level && score >= 10000){
            speedUpgrade += 0.25;
            level ++;
        }

    }
    function displayText(context){
        context.fillStyle = 'black';
        context.font = '24px Helvetica';
        context.fillText('Score: ' + score, 20, 50);
        context.fillStyle = 'black';
        context.font = '24px Helvetica';
        context.fillText('Bonus: ' + bottlesCaught, 850, 50);
    }
    function gameOverText(context){
        context.fillStyle = 'black';
        context.font = '48px Helvetica';
        context.textAlign = 'center';
        context.textBaseline = 'middle';
        context.fillText('GAME OVER', canvas.width/2, canvas.height/2 - 50);
        if (localStorage.getItem("city") != null){
        context.fillText(`${localStorage.getItem("pseudo")}, ${localStorage.getItem("city")} - Score: ${score}`, canvas.width/2, canvas.height/2 + 50)
            }
        else {
            context.fillText(`${localStorage.getItem("pseudo")} - Score: ${score}`, canvas.width/2, canvas.height/2 + 50)
        }
    }
    function welcomeText(context){
        context.fillStyle = 'black';
        context.font = '48px Helvetica';
        context.textAlign = 'center';
        context.textBaseline = 'center';
        context.fillText('WELCOME', canvas.width/2, canvas.height/2);
    }

    function forward() {
        coach.x += 1;
    }
    function backward(){
        coach.x -= 1;
    }
    function repeat(func, times) {
        func();
        times && --times && repeat(func, times);
    }
    /* ------------------------------------------------ */


    /* ----------------- LOCALISATION ----------------- */
    function getLocation(){
        if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(success, postionError);
            } else {
               console.log("Geolocation is not supported by this browser.");
            }
    }
    function success(pos) {
        let crd = pos.coords;
        let lat = crd.latitude.toString();
        let lng = crd.longitude.toString();
        let coordinates = [lat, lng];

        console.log('Your current position is:');
        console.log(`Latitude : ${lat}`);
        console.log(`Longitude: ${lng}`);
        console.log(`More or less ${crd.accuracy} meters.`);

        getCity(coordinates);
        return;
    }
    function postionError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                console.log("User denied the request for geolocation")
                break;
            case error.POSITION_UNAVAILABLE:
                console.log("Location information is unavailable")
                break;
            case error.TIMEOUT:
                console.log("The request to get user location timed out")
                break;
            case error.UNKNOWN_ERROR:
                console.log("An unknown error occured")
                break;
        }
    }
    function getCity(coordinates){
        var xhr = new XMLHttpRequest();
        var lat = coordinates[0];
        var lng = coordinates[1];


        xhr.open('GET', "https://us1.locationiq.com/v1/reverse.php?key=pk.aa7037a8815df5ec28e1507e14e1f0bf&lat=" +
            lat + "&lon=" + lng + "&format=json", true);
        xhr.send();
        xhr.onreadystatechange = processRequest;
        xhr.addEventListener("readystatechange", processRequest, false);

        function processRequest(e) {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = JSON.parse(xhr.responseText);
                var city = response.address.town;
                localStorage.setItem("city",city);
                console.log(`City: ${city}`);
                return;
            }
        }
    }
    getLocation();
    /* ------------------------------------------------ */

    /* -------------- INITIATING CLASSES -------------- */
    const input = new InputHandler();
    const badboy = new Badboy(canvas.width, canvas.height);
    const coach = new Coach(canvas.width, canvas.height);
    const background = new Background(canvas.width, canvas.height);
    const background1 = new Background1(canvas.width, canvas.height);
    const backgroundend = new BackgroundEnd(canvas.width, canvas.height);
    const scoreboard = new Score(canvas.width, canvas.height);
    const stadium = new Stadium(canvas.width, canvas.height);
    const button = document.createElement('button');
    /* ----------------------------------------------- */

    function welcome(){
        ctx.clearRect(0,0,canvas.width, canvas.height);
        stadium.draw(ctx);
        dragndropModal.style.display="none";
        scoreModal.style.display="none";
        start.onclick = () => {
            loginModal.style.display = "none";
            animate(0);
        }
    }

    let lastTime = 0;
    let obstacleTimer = 0;
    let obstacleInterval = 500;
    let randomObstacleInterval = Math.random() * 1500 + 500;
    let birdTimer = 0;
    let birdInterval = 2000;
    let randomBirdInterval = Math.random()*3500 + 1000;
    let bottleTimer = 0;
    let bottleInterval = 1000;
    let randomBottleInterval = Math.random() * 2000 + 500;
    let counter = 0;

    // DRAG'N'DROP
    let bottleEnd = document.getElementById("bottleEnd");
    bottleEnd.setAttribute('draggable', true);
    bottleEnd.ondragstart = function(e){
        this.style.opacity = '0.4';
    };
    bottleEnd.ondragend = function(e){
        this.style.opacity='1';
    };
    let panier = document.getElementById("panier");
    panier.ondragover = function(e) {
        return false;
    };
    panier.ondrop = function(e){
        e.preventDefault;
        switch(counter){
            case 0:
                this.src="ressources/images/game/panier1.png";
                break;
            case 1:
                this.src="ressources/images/game/panier2.png";
                break;
            case 2:
                this.src="ressources/images/game/panier3.png";
                break;
            case 3:
                this.src="ressources/images/game/panier4.png";
                break;
            case 4:
                this.src="ressources/images/game/panier5.png";
                bottleEnd.style.display = "none";
                ratingFont.style.display = "none";
                thanks.style.display = "block";
                playAgain.style.display = "inline-block";
                break;
            default:
                /* play again*/
                /* delete bottle*/
        }
        counter++;
    };

    function animate(timeStamp){
        ctx.clearRect(0,0,canvas.width, canvas.height);
        music.play();
        const deltaTime = timeStamp - lastTime;
        lastTime = timeStamp;
        handleSpeed();
        background1.draw(ctx);
        background1.update();
        background.draw(ctx);
        background.update();
        displayText(ctx);
        if(score >= 50){
            handleBottles(deltaTime);
        }
        handleBoards();
        coach.draw(ctx);
        coach.update(input, deltaTime, obstacles, birds, bottles, badboy);
        badboy.draw(ctx);
        badboy.update(input, deltaTime);
        if(score >= 50){
            handleObstacles(deltaTime);
        }
        if(score >= 4000){
            handleBirds(deltaTime);
        }
        score++;
        if(!gameOver){
            requestAnimationFrame(animate);
        }
        if(gameOver){
            scoreboard.draw(ctx);
            gameOverText(ctx);
            scoreModal.style.display="block";
            toLeaderboard.onclick = () => {
                leaderboard();
            }
        }
    }
    function leaderboard(){
        scoreModal.style.display="none";
        backgroundend.draw(ctx);
        dragndropModal.style.display = "block";
        thanks.style.display = "none";
    }
    welcome();

    /* ----------- PSEUDO ENTRY & STORAGE ----------- */
    if (localStorage.getItem("pseudo")!=null){
        login.style.display = "none";
        hi.textContent = `Hi ${localStorage.getItem("pseudo")}!`;
        pChange.onclick = () => {
            whistle.play();
            localStorage.clear("pseudo");
            document.location.reload();
        }
    }

    if (localStorage.getItem("pseudo") == null){
        change.style.display = "none";
        bLogin.onclick = () =>{
            localStorage.setItem("pseudo", pseudo.value);
            document.location.reload();
        }
    }

    playAgain.onclick = () =>{
        document.location.reload();
        animate(timeStamp);
    }

});
